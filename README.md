# add-application-deadline

Pipeline step that tries to detect application deadline for an ad and adds the value if found, for example:  
"application_deadline": "2020-10-31T23:59:59"

If the scraped attribute "validThrough" is found, this value will be used, otherwise dates are detected in 
the "description" value.

## Prerequisites  
Docker and a bash shell  
  
## Needed input data for running  
The step needs a list of ads with the attributes/structure:    
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 1 här" }}  
{"originalJobPosting": {"description": "Annonsbeskrivningen för annons 2 här" }}  

Optional, with "validThrough":  
{"originalJobPosting": {"validThrough":"2020-11-11", "description": "Annonsbeskrivningen för annons 3 här" }}
  
## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/add_application_deadline_output  
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/add-application-deadline sh  
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir and run with:    
python main.py --filepath resources/thepipelinefile  