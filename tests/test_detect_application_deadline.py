import os
import sys
import pytest

from main import detect_application_deadline_in_ad
from main import detect_application_deadline_in_text
from main import detect_application_deadline_in_scraped_date
# from main import clean_day_strings
from main import search_dates_in_text


# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date1():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'En mening innan. Ansök senast 24 januari. En mening efter'
    output = detect_application_deadline_in_text(input)

    assert output.month == 1
    assert output.day == 24
    assert output.hour == 23
    assert output.minute == 59
    assert output.second == 59

# # @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_with_number_and_character():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Ansök senast den 24e December.'
    output = detect_application_deadline_in_text(input)

    assert output.month == 12
    assert output.day == 24

    input = 'Ansök senast den 24:e December.'
    output = detect_application_deadline_in_text(input)

    assert output.month == 12
    assert output.day == 24

    input = 'Ansök senast den 22a December.'
    output = detect_application_deadline_in_text(input)

    assert output.month == 12
    assert output.day == 22

    input = 'Ansök senast den 22:a December.'
    output = detect_application_deadline_in_text(input)

    assert output.month == 12
    assert output.day == 22


@pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_with_six_digits():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    # TODO: Wait for answer for https://github.com/scrapinghub/dateparser/issues/833 and try test again.
    input = 'Ansök senast 20321224'
    output = detect_application_deadline_in_text(input)

    assert output.year == 2032
    assert output.month == 12
    assert output.day == 24
    


# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_with_day_month_and_year():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Ansök senast den 24 december 2032'
    output = detect_application_deadline_in_text(input)

    assert output.year == 2032
    assert output.month == 12
    assert output.day == 24

# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_linebreak():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Text med radbrytning\nAnsökningsdatum: 2022-04-01'
    output = detect_application_deadline_in_text(input)

    assert output.month == 4
    assert output.day == 1

# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_in_scraped_data_only():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = '2022-04-01'
    output = detect_application_deadline_in_scraped_date(input)

    assert output.year == 2022
    assert output.month == 4
    assert output.day == 1


# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_in_scraped_data():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = {"originalJobPosting": {"validThrough":"2020-11-23", "description": "Ansök senast den 24 december 2032" }}
    output = detect_application_deadline_in_ad(input)

    assert output.year == 2020
    assert output.month == 11
    assert output.day == 23

# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_scraped_data_empty():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = {"originalJobPosting": {"validThrough": None, "description": "Ansök senast den 24 december 2032" }}
    output = detect_application_deadline_in_ad(input)

    assert output.year == 2032
    assert output.month == 12
    assert output.day == 24

# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_scraped_data_not_valid():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = {"originalJobPosting": {"validThrough": "TEST", "description": "Ansök senast den 24 december 2032" }}
    output = detect_application_deadline_in_ad(input)

    assert output.year == 2032
    assert output.month == 12
    assert output.day == 24

# @pytest.mark.skip(reason="Temporarily disabled")
def test_detect_date_year_bug():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = '''Ansök med 1337 ansökningar och ansök senast den 20 augusti.'''
    output = detect_application_deadline_in_text(input)

    assert output.year != 1337 and output.year != 1338

# # @pytest.mark.skip(reason="Temporarily disabled")
# def test_clean_day_strings():
#     print('============================', sys._getframe().f_code.co_name, '============================ ')
#
#     wanted_output = 'Ansök senast den 24 december 2032'
#     assert clean_day_strings('Ansök senast den 24:e december 2032') == wanted_output
#     assert clean_day_strings('Ansök senast den 24e december 2032') == wanted_output
#     assert clean_day_strings('Ansök senast den 24 december 2032') == wanted_output
#     assert clean_day_strings('Test\nAnsök senast den 24:e december 2032') == 'Test\n' + wanted_output
#
#     wanted_output2 = 'Ansök senast den 22 december 2032'
#     assert clean_day_strings('Ansök senast den 22:a december 2032') == wanted_output2
#     assert clean_day_strings('Ansök senast den 22a december 2032') == wanted_output2
#     assert clean_day_strings('Ansök senast den 22 december 2032') == wanted_output2


if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])