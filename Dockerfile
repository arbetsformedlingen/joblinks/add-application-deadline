FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app
COPY . /app

RUN apt-get -y update &&\
    apt-get -y install python3 python3-pip &&\
        python3 -m pip install --upgrade pip && python3 -m pip install -r requirements.txt && \
        # Download and install nltk.punkt-tokenizer
        python3 -m pip install nltk && python3 -c "import nltk; nltk.download('punkt', download_dir='/usr/share/nltk_data')"


###############################################################################
FROM base AS test

COPY tests/ /tests/

RUN  python3 -m pytest -vv tests/ \
     && python3 main.py --filepath /tests/testdata/input.10 > /tmp/output.10 \
     && diff -s /tests/testdata/expected_output.10 /tmp/output.10 \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD ["python3","main.py"]
