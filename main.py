import argparse
import datetime
import os
import re
import sys

import ndjson
import nltk
import pandas as pd
from dateparser.search import search_dates
from dateparser_data.settings import default_parsers

swedish_punkt_tokenizer = nltk.data.load('nltk:tokenizers/punkt/swedish.pickle')


def read_ads_stream_from_std_in():
    reader = ndjson.reader(sys.stdin)
    for ad in reader:
        yield ad


def read_ads_from_file(rel_filepath):
    # Open input file with ads and load ndjson to dictionary...
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    ads_path = currentdir + rel_filepath
    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads


def print_ad(ad):
    output_json = ndjson.dumps([ad], ensure_ascii=False)
    print(output_json, file=sys.stdout)


def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)



def tokenize_to_sentences(ad_description):
    # some ads have job description in brackets
    if isinstance(ad_description, list):
        ad_description = ad_description[0]

    all_sentences = []
    sections = ad_description.split('\n')
    for section in sections:
        sentences = swedish_punkt_tokenizer.tokenize(section)
        sentences = [sentence for sentence in sentences if sentence]
        all_sentences.extend(sentences)
    return all_sentences

RE_BAD_PARSE_STR = re.compile(r'[$&+,:;=?@#|<>^*()%!]')

def detect_application_deadline_in_text(text_to_search):

    clean_text_to_search = clean_day_strings(text_to_search)
    found_dates = search_dates_in_text(clean_text_to_search)

    found_application_deadline = None

    if found_dates:
        for found_date in found_dates:
            parsed_string = found_date[0]
            found_datetime = found_date[1]

            if not len(parsed_string) < 4 and not RE_BAD_PARSE_STR.search(parsed_string):
                # print('%s - %s' % (parsed_string, found_datetime))
                tokenized_sentences = tokenize_to_sentences(clean_text_to_search)
                sentences = [sentence.lower() for sentence in tokenized_sentences if parsed_string in sentence]
                sentences = pd.Series(sentences)

                # TODO: Varianter på engelska?
                mandatory_prefix = ["ansökningsdag", "Sista ansökningsdag", "ansökan senast", "ansökningsdatum",
                                    "Sista ansökningsdatum", "ansök senast", "Sista ansökan", "inkommit senast"]
                mandatory_prefix = [term.lower() for term in mandatory_prefix]
                sentences_with_mandatory_prefix = sentences[sentences.astype(str).str.contains('|'.join(mandatory_prefix))]

                if not sentences_with_mandatory_prefix.empty:
                    found_application_deadline = set_last_minute_time(found_datetime)

    return found_application_deadline


def set_last_minute_time(found_datetime):
    return found_datetime.replace(hour=23, minute=59, second=59)


def detect_application_deadline_in_scraped_date(datestring):
    clean_text_to_search = clean_day_strings(datestring)
    found_dates = search_dates_in_text(clean_text_to_search)

    if found_dates and len(found_dates) == 1:
        found_date = found_dates[0]
        found_datetime = found_date[1]
        return set_last_minute_time(found_datetime)
    else:
        return None


def search_dates_in_text(text_to_search):
    langs = ['sv', 'en']
    parsers = [parser for parser in default_parsers if parser != 'relative-time']
    settings = {'PREFER_DAY_OF_MONTH': 'first', 'PREFER_DATES_FROM': 'future', 'RELATIVE_BASE': datetime.datetime.now(),
                'REQUIRE_PARTS': ['day'], 'PARSERS': parsers, 'RETURN_AS_TIMEZONE_AWARE': False}
    found_dates = search_dates(text_to_search, languages=langs, settings=settings, add_detected_language=False)
    return found_dates


RE_DAY_STR = re.compile(r"(\d{1,2}):{0,1}[ae]", re.UNICODE)

def clean_day_strings(description):
    return RE_DAY_STR.sub(r"\1", description)


def detect_application_deadline_in_ad(ad):

    found_application_deadline = None

    if 'originalJobPosting' in ad:
        if 'validThrough' in ad['originalJobPosting'] and ad['originalJobPosting']['validThrough']:
            scraped_application_deadline = ad['originalJobPosting']['validThrough']
            found_application_deadline = detect_application_deadline_in_scraped_date(scraped_application_deadline)

        if not found_application_deadline and 'description' in ad['originalJobPosting'] \
                and ad['originalJobPosting']['description']:
            description = ad['originalJobPosting']['description']
            if isinstance(description, list):
                description = description[0]
            found_application_deadline = detect_application_deadline_in_text(description)

    return found_application_deadline


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Detects the language in the description for the ad')
    parser.add_argument('--filepath', help='Optional relative filepath to read ads from', required=False)

    args = parser.parse_args()


    filepath = args.filepath

    if filepath:
        ads = read_ads_from_file(filepath)
    else:
        ads = read_ads_stream_from_std_in()


    for i, ad in enumerate(ads):
        try:
            found_application_deadline = detect_application_deadline_in_ad(ad)

            if found_application_deadline:
                ad['application_deadline'] = found_application_deadline.isoformat()

            print_ad(ad)

        except Exception as e:
            print_error(e)


